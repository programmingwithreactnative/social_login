import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {LoginManager} from 'react-native-fbsdk';

class App extends Component{
    fbAuth(){
        LoginManager.logInWithReadPermissions(['public_profile']).then(
            (result)=>{
                if(result.isCancelled){
                    console.log("cancelled");
                }else{
                    console.log("login success. granted permissions: " + result.grantedPermissions.toString());
                }
            }, (error)=> {
                console.log("error: " + error);   
            }
        );
    }
    render(){
        return(
                <View>
                    <TouchableOpacity onPress={this.fbAuth}>
                        <Image 
                        source={require('./img/fb-login-button.png')}
                        style={{borderColor:'black', borderWidth:3}}
                        />
                    </TouchableOpacity>
                </View>
        );
    }
}
export default App;